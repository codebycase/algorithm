package s01_design_bit_array;

/**
 * <h3>Question:</h3>
 * <p>
 * An array of int can be used to deal with array of bits. Assuming size of int to be 4 bytes, when we talk about an int, we are dealing with 32 bits. Say we have int A[10], means we are working on 10
 * * 4 * 8 = 320 bits.
 * </p>
 * 
 * <h3>Answer:</h3>
 * <p>
 * </p>
 * http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/1-C-intro/bit-array.html
 * 
 * @author lchen
 *
 */
public class DesignBitArray {
	private int[] a;

	// give a big enough array: n * 4 * 8
	public DesignBitArray(int[] a) {
		this.a = a;
	}

	public void setBit(int k) {
		a[k / 32] |= 1 << (k % 32);
	}

	public void clearBit(int k) {
		a[k / 32] &= ~(1 << (k % 32));
	}

	public boolean testBit(int k) {
		return (a[k / 32] & (1 << (k % 32))) != 0;
	}
}
